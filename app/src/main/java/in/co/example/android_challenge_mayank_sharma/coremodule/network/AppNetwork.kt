package `in`.co.example.android_challenge_mayank_sharma.coremodule.network

import `in`.co.example.android_challenge_mayank_sharma.MyApplication
import `in`.co.example.android_challenge_mayank_sharma.coremodule.util.HelperClass
import okhttp3.Cache
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object AppNetwork {


    private var mRetrofit: Retrofit? = null
    val HTTPS_BASE_URL = "http://www.mocky.io/v2/"
    val CONNECT_TIMEOUT: Long = 30000
    val READ_TIMEOUT: Long = 30000
    val WRITE_TIMEOUT: Long = 30000


    fun getRetrofitInstance(): Retrofit? {

        if (mRetrofit == null){
            mRetrofit = Retrofit.Builder()
                .baseUrl(HTTPS_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(getOKHTTPClient())
                .build()
        }

        return mRetrofit
    }





    private fun getOKHTTPClient(): OkHttpClient {

        val cacheSize = (5 * 1024 * 1024).toLong()
        val myCache = Cache(MyApplication.appContext.cacheDir, cacheSize)

        val okHttpClient = OkHttpClient.Builder()
            .cache(myCache)
            .addInterceptor { chain ->
                var request = chain.request()
                request = if (HelperClass.hasNetwork(MyApplication.appContext)!!)
                    request.newBuilder().header("Cache-Control", "public, max-age=" + 5).build()
                else
                    request.newBuilder().header("Cache-Control", "public, only-if-cached, max-stale=" + 60 * 60 * 24 * 7).build()
                chain.proceed(request)
            }
            .build()


        return okHttpClient
    }
}