package `in`.co.example.android_challenge_mayank_sharma.coremodule.util

import `in`.co.example.android_challenge_mayank_sharma.MyApplication
import `in`.co.example.android_challenge_mayank_sharma.model.Fares
import android.text.format.DateFormat
import android.widget.TextView
import androidx.databinding.BindingAdapter
import java.util.*




object CustomBindingAdapter {





    @BindingAdapter("app:timeConverter")
    @JvmStatic
    fun convertTimeDate(view: TextView, time: Long) {

        view.text =  getTimeFromTimeStamp(time)
    }

    private fun getTimeFromTimeStamp(millisecond: Long): String? {

        val datetimeString : String = DateFormat.format("MM-dd-yyyy hh:mm:ss a", Date(millisecond)).toString()
        val  timeString :String = datetimeString.substring(11,16)

        return timeString
    }

    @BindingAdapter("app:showFare")
    @JvmStatic
    fun setMinFare(view: TextView, list : List<Fares>) {

       val sortedList: List<Fares> = list.sortedWith(compareBy { it.fare })

        view.text  = sortedList?.get(0)?.fare.toString()

    }


}