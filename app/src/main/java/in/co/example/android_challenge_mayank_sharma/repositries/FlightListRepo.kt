package `in`.co.example.android_challenge_mayank_sharma.repositries

import `in`.co.example.android_challenge_mayank_sharma.coremodule.network.AppNetwork
import `in`.co.example.android_challenge_mayank_sharma.model.FlightData
import android.util.Log
import androidx.lifecycle.MutableLiveData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FlightListRepo  {

    private val flightListServices : FlightListServices? = AppNetwork.getRetrofitInstance()?.create(FlightListServices::class.java)



    fun getFlightData( data: MutableLiveData<FlightData>){


        flightListServices?.getFlightList()?.enqueue(object : Callback<FlightData> {

            override fun onResponse(call: Call<FlightData>, response: Response<FlightData>) {
                data.value = response.body()
            }

            override fun onFailure(call: Call<FlightData>, t: Throwable) {
                // TODO better error handling in part #2 ...
                data.value=null
            }
        })
    }
}