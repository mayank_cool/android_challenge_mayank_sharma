package `in`.co.example.android_challenge_mayank_sharma.view.fragments

import `in`.co.example.android_challenge_mayank_sharma.coremodule.BaseClasses.BaseFragment
import `in`.co.example.android_challenge_mayank_sharma.databinding.FlightListFragmentBinding
import `in`.co.example.android_challenge_mayank_sharma.view.adapter.FlightListFragmentAdapter
import `in`.co.example.android_challenge_mayank_sharma.viewmodel.FlightListViewModel
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders

class FlightListFragment: BaseFragment() {

    private lateinit var binding : FlightListFragmentBinding
    private lateinit var viewModelUser: FlightListViewModel
    private lateinit var adapter : FlightListFragmentAdapter<Any?>


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = FlightListFragmentBinding.inflate(inflater, container, false)
        binding.toolbar.setTitle("Flight")
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModelUser = activity?.run {
            ViewModelProviders.of(this)[FlightListViewModel::class.java]
        } ?: throw Exception("Invalid Activity") as Throwable

       binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModelUser
        initAdapter()
        renderFlightListData()
    }

    private  fun initAdapter(){
        adapter = FlightListFragmentAdapter(null)
        binding.adapter = adapter

    }

    private fun renderFlightListData(){

        viewModelUser.getFlightData()?.observe(viewLifecycleOwner , Observer {
            viewModelUser.showLoader.value = false

            if (it != null ){
                binding.toolbar.setTitle(viewModelUser.getTitle())
                adapter.refreshData(it,true)
                binding.isEmptyList = false
            }else{
                binding.isEmptyList = true
            }
        })
    }

}