package `in`.co.example.android_challenge_mayank_sharma.view.adapter

import `in`.co.example.android_challenge_mayank_sharma.R
import `in`.co.example.android_challenge_mayank_sharma.coremodule.util.HelperClass
import `in`.co.example.android_challenge_mayank_sharma.databinding.FlightListAdapterBinding
import `in`.co.example.android_challenge_mayank_sharma.model.Fares
import `in`.co.example.android_challenge_mayank_sharma.model.FlightData
import `in`.co.example.android_challenge_mayank_sharma.model.Flights
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView

class FlightListFragmentAdapter<T>(private var flightData: FlightData?) : RecyclerView.Adapter<FlightListFragmentAdapter<T>.Holder>() {



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FlightListFragmentAdapter<T>.Holder {
        val binding =
            DataBindingUtil.inflate<ViewDataBinding>(LayoutInflater.from(parent.context), R.layout.flight_list_adapter, parent, false)
        return Holder(binding)
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        var binding : FlightListAdapterBinding = holder.binding as FlightListAdapterBinding
        var flightInfo : Flights? =  flightData?.flights?.get(position)
        binding.flightInfo = flightInfo
        binding.adapter = this
        binding.calculatedTime = HelperClass.convertFromDuration((flightInfo?.arrivalTime ?:0).minus(flightInfo?.departureTime ?:0)).toString()
        binding.airlineName = flightInfo?.airlineCode?.let { getFlightName(it) }
    }


    fun  getFlightName( flightCode:String) :String?{

       return  flightData?.appendix?.airline?.get(flightCode)
    }

    override fun getItemCount(): Int {
        return flightData?.flights?.size ?: 0
    }

    @JvmOverloads
    fun refreshData(flightData: FlightData?, refreshList: Boolean = true) {
        this.flightData = flightData
        if (refreshList)
            notifyDataSetChanged()
    }

    fun onItemClicked(view: View, flight: Flights){
        val bundle : Bundle = Bundle()
        bundle.putParcelable("value",flight)
        Navigation.findNavController(view).navigate(R.id.action_list_to_detail_frag,bundle)

    }
    inner class Holder(var binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root)

}