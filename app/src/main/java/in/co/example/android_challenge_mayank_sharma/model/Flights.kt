package `in`.co.example.android_challenge_mayank_sharma.model

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class Flights (
    @SerializedName("originCode")
    @Expose
    var  originCode :String ,
    @SerializedName("destinationCode")
    @Expose
    var   destinationCode :String,
    @SerializedName("departureTime")
    @Expose
    var  departureTime : Long,
    @SerializedName("arrivalTime")
    @Expose
    var  arrivalTime : Long,
    @SerializedName("airlineCode")
    @Expose
    var airlineCode :String,
    @SerializedName("class")
    @Expose
     var _class : String,
    @SerializedName("fares")
    @Expose
    var   fares:List<Fares>
): Parcelable