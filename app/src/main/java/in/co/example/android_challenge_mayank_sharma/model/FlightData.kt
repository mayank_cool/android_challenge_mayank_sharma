package `in`.co.example.android_challenge_mayank_sharma.model

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
 data class FlightData (
    @SerializedName("appendix")
    @Expose
    var  appendix : Appendix ,
    @SerializedName("flights")
    @Expose
    var  flights : List<Flights>
): Parcelable
