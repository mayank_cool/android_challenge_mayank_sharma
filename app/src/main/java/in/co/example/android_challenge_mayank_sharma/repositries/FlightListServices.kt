package `in`.co.example.android_challenge_mayank_sharma.repositries

import `in`.co.example.android_challenge_mayank_sharma.model.FlightData
import retrofit2.Call
import retrofit2.http.GET

interface FlightListServices {

    @GET("5979c6731100001e039edcb3")
    fun getFlightList(): Call<FlightData>
}