package `in`.co.example.android_challenge_mayank_sharma.model

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class Appendix (
    @SerializedName("airlines")
    @Expose
    var airline :  HashMap<String, String> ,

    @SerializedName("airports")
    @Expose
    var airports :  HashMap<String, String>,
    @SerializedName("providers")
    @Expose
    var providers :  HashMap<String, String>
): Parcelable