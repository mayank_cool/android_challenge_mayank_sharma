package `in`.co.example.android_challenge_mayank_sharma.view.adapter

import `in`.co.example.android_challenge_mayank_sharma.R
import `in`.co.example.android_challenge_mayank_sharma.databinding.FareListAdapterBinding
import `in`.co.example.android_challenge_mayank_sharma.databinding.FlightListAdapterBinding
import `in`.co.example.android_challenge_mayank_sharma.model.Fares
import `in`.co.example.android_challenge_mayank_sharma.model.FlightData
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import android.icu.lang.UCharacter.GraphemeClusterBreak.T




class FareListAdapter<T>(private var fareList: List<Fares>?, private var providers :  HashMap<String, String>?): RecyclerView.Adapter<FareListAdapter<T>.Holder>() {


    var mSelectedItem : Int? = getMinFareSelection()



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FareListAdapter<T>.Holder {

        val binding =
            DataBindingUtil.inflate<ViewDataBinding>(LayoutInflater.from(parent.context), R.layout.fare_list_adapter, parent, false)
        return Holder(binding)
    }

    override fun getItemCount(): Int {

        return fareList?.size ?: 0
    }

    override fun onBindViewHolder(holder: FareListAdapter<T>.Holder, position: Int) {
        var binding : FareListAdapterBinding = holder.binding as FareListAdapterBinding

        var fareData : Fares? =  fareList?.get(position)
        binding.fare = fareData?.fare.toString()
        binding.adapter = this
        binding.position = position
        binding.providerName =  providers?.get(fareData?.providerId.toString())
        binding.isChecked = position == mSelectedItem ?: false

    }


    fun onItemChecked(view: View, position: Int){
         mSelectedItem = position
         notifyDataSetChanged()
    }

    fun getMinFareSelection(): Int? {
       var fares  =   fareList?.minBy { it ->  it.fare.toInt() }
          return    fareList?.indexOf(fares)
    }

    inner class Holder(var binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root)

}