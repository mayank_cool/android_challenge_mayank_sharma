package `in`.co.example.android_challenge_mayank_sharma

import android.app.Application
import android.content.Context

class MyApplication : Application() {


    override fun onCreate() {
        super.onCreate()
        appContext = applicationContext
    }

    companion object {

        lateinit  var appContext: Context

    }
}