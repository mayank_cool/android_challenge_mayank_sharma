package `in`.co.example.android_challenge_mayank_sharma.viewmodel

import `in`.co.example.android_challenge_mayank_sharma.coremodule.util.HelperClass
import `in`.co.example.android_challenge_mayank_sharma.model.FlightData
import `in`.co.example.android_challenge_mayank_sharma.model.Flights
import `in`.co.example.android_challenge_mayank_sharma.repositries.FlightListRepo
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch



class FlightListViewModel : ViewModel() {

    var showLoader =  MutableLiveData<Boolean>()
    private val  listRepo = FlightListRepo()

    // variables for detail fragment
    var departurePlaceName =  MutableLiveData<String>()
    var arrivalPlaceName =  MutableLiveData<String>()
    var classType =  MutableLiveData<String>()
    var calculatedTime =  MutableLiveData<String>()
    var formattedDate =  MutableLiveData<String>()
    var arrivalFormattedDate =  MutableLiveData<String>()
    var modifiedFlightName =  MutableLiveData<String>()
    var departureCodeWithTime =  MutableLiveData<String>()
    var arrivalCodeWithTime =  MutableLiveData<String>()


    private val flightData: MutableLiveData<FlightData> by lazy {
        MutableLiveData<FlightData>().also {
            loadDataList()
        }
    }


    private fun loadDataList() {
        showLoader.value = true
        viewModelScope.launch {
            listRepo.getFlightData(flightData)

        }

    }


    fun getFlightData(): LiveData<FlightData>? {
        return flightData
    }

    fun initVariables( selectedFlights: Flights){
        departurePlaceName.value = flightData.value?.appendix?.airports?.get(selectedFlights.originCode)
        arrivalPlaceName.value = flightData.value?.appendix?.airports?.get(selectedFlights.destinationCode)
        classType.value =  selectedFlights._class
        modifiedFlightName.value = flightData.value?.appendix?.airline?.get(selectedFlights.airlineCode)

        calculatedTime.value = HelperClass.convertFromDuration((selectedFlights?.arrivalTime ?:0).minus(selectedFlights?.departureTime ?:0)).toString()
        formattedDate.value = HelperClass.getTimeAndDateFromTimeStamp(selectedFlights.departureTime,false)
        arrivalFormattedDate.value = HelperClass.getTimeAndDateFromTimeStamp(selectedFlights.arrivalTime,false)
        departureCodeWithTime.value = selectedFlights.originCode +" " +  HelperClass.getTimeAndDateFromTimeStamp(selectedFlights.departureTime,true)
        arrivalCodeWithTime.value = selectedFlights.destinationCode +" " +  HelperClass.getTimeAndDateFromTimeStamp(selectedFlights.arrivalTime,true)
    }


    fun getProviderInfo():HashMap<String,String> ?{

      return  flightData.value?.appendix?.providers
    }

    fun getTitle():String? {

        return flightData.value?.flights?.get(0)?.originCode + " To "+  flightData.value?.flights?.get(0)?.destinationCode
    }
}