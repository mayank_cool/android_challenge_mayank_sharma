package `in`.co.example.android_challenge_mayank_sharma.view.fragments

import `in`.co.example.android_challenge_mayank_sharma.R
import `in`.co.example.android_challenge_mayank_sharma.coremodule.BaseClasses.BaseFragment
import `in`.co.example.android_challenge_mayank_sharma.databinding.FlightDetailFragmentBinding
import `in`.co.example.android_challenge_mayank_sharma.databinding.FlightListFragmentBinding
import `in`.co.example.android_challenge_mayank_sharma.model.Flights
import `in`.co.example.android_challenge_mayank_sharma.view.adapter.FareListAdapter
import `in`.co.example.android_challenge_mayank_sharma.view.adapter.FlightListFragmentAdapter
import `in`.co.example.android_challenge_mayank_sharma.viewmodel.FlightListViewModel
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.flight_detail_fragment.*

class FlightDetailFragment  : BaseFragment() {


    private lateinit var binding :FlightDetailFragmentBinding
    private lateinit var viewModel: FlightListViewModel
    private var  selectedFlightInfo : Flights? = null
    private var  providerInfo :  HashMap<String, String>? = null
    private lateinit var adapter : FareListAdapter<Any?>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        selectedFlightInfo  = arguments?.getParcelable("value")
        binding = FlightDetailFragmentBinding.inflate(inflater, container, false)
        binding.toolbar.setTitle("Flight Detail")
        return binding.root
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = activity?.run {
            ViewModelProviders.of(this)[FlightListViewModel::class.java]
        } ?: throw Exception("Invalid Activity") as Throwable

        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel
        selectedFlightInfo?.let { viewModel.initVariables(it) }
        providerInfo = viewModel.getProviderInfo()
        initAdapter()
    }

    private  fun initAdapter(){
        adapter = FareListAdapter(selectedFlightInfo?.fares,providerInfo)
        binding.adapter = adapter

    }



}