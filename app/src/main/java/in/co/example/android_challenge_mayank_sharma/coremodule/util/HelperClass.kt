package `in`.co.example.android_challenge_mayank_sharma.coremodule.util

import `in`.co.example.android_challenge_mayank_sharma.MyApplication
import `in`.co.example.android_challenge_mayank_sharma.view.adapter.FlightListFragmentAdapter
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.text.format.DateFormat
import android.widget.Toast
import java.util.*

object HelperClass {


    fun hasNetwork(context: Context): Boolean? {
        var isConnected: Boolean? = false // Initial Value
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = connectivityManager.activeNetworkInfo
        if (activeNetwork != null && activeNetwork.isConnected)
            isConnected = true
        return isConnected
    }


    fun convertFromDuration(timeInMilli: Long): TimeInHours {
        val timeInSeconds :Long  = timeInMilli/1000
        var time = timeInSeconds
        val hours = time / 3600
        time %= 3600
        val minutes = time / 60
        time %= 60
        val seconds = time
        return TimeInHours(hours.toInt(), minutes.toInt(), seconds.toInt())
    }

    class TimeInHours(val hours: Int, val minutes: Int, val seconds: Int) {
        override fun toString(): String {
            //return String.format("%dh : %02dm : %02ds", hours, minutes, seconds)
            return String.format("%dh : %02dm ", hours, minutes)
        }
    }

     fun getTimeAndDateFromTimeStamp(millisecond: Long, isForTime : Boolean): String? {
        val modifiedDateTime : String ?
        val datetimeString : String = DateFormat.format("MM-dd-yyyy hh:mm:ss a", Date(millisecond)).toString()
        if (isForTime)
         modifiedDateTime = datetimeString.substring(11,16)
        else
            modifiedDateTime =  datetimeString.substring(0,11)

        return modifiedDateTime
    }

    fun showToast(msg : String){

        Toast.makeText(MyApplication.appContext,msg, Toast.LENGTH_LONG).show()
    }
}